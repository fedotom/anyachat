const http = require('http');
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const config = require('config');
const mongoose = require('mongoose');
const { Server } = require('socket.io');

const app = express();
const server = http.createServer(app);
const io = new Server(server, {
  cors: {
    origin: '*'
  }
});

const { add } = require('./routes/message.route');

io.on('connection', (socket) => {
  socket.on('chat message', (msg) => {
    add(msg, io);
  });
});

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));

app.use('/', require('./api'));

const start = async () => {
  try {
    await mongoose.connect(config.get('MONGO_URI'));

    const PORT = config.get('PORT') || 8000;
    server.listen(PORT, () => console.log(`Listening on port ${PORT}`));
  } catch (err) {
    console.error(err.message);
    process.exit(1);
  }
};

start();

