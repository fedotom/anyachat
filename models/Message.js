const { Schema, model, Types } = require('mongoose');

const schema = new Schema({
  message: { type: String, required: true },
  userId: { type: Types.ObjectId, required: true },
  createdAt: { type: Date, default: Date.now },
});

module.exports = model('Message', schema)
