import React, { useEffect, useState } from 'react';
import postData from '../utils/postData';

const Profile = () => {
  const [name, setName] = useState('');
  const [lastname, setLastname] = useState('');
  const [email, setEmail] = useState('');

  useEffect(async () => {
    try {
      const snuser = localStorage.getItem('snuser');
      const user = JSON.parse(snuser);
  
      const profile = await postData('/user/profile', user);
  
      if (profile) {
        setName(profile.name);
        setLastname(profile.lastname);
        setEmail(profile.email);
        
      } else if (profile.error) {
        window.M.toast({ html: profile.error });
      }
    } catch (err) {
      console.error(err.message);
    }
  }, []);

  return (
    <>
      <div className="container">
        <h1>Profile</h1>
        <p>
          <h4>{name} {lastname}</h4>
          <br />
          <span>Email: {email}</span>
        </p>
      </div>
    </>
  );
};

export default Profile;
