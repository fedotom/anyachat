import './Chat.css';
import React, { useCallback, useEffect, useState, useRef } from 'react';
import io from 'socket.io-client';

const Chat = () => {
  const [messages, setMessages] = useState([]);
  const socketRef = useRef(null);

  useEffect(async () => {
    try {
      socketRef.current = io('http://localhost:8000');

      socketRef.current.on('chat message', (msg) => {
        // const copied = [...messages];
        // copied.push(msg);
        messages.push(msg);
        setMessages([ ...messages ]);
      });

      const response = await fetch('/message');
      const messages = await response.json();
      setMessages([...messages]);
    } catch (err) {
      console.error(err.message);
    }
  }, []);

  const onFormSubmit = useCallback(async (e) => {
    try {
      e.preventDefault();

      const snuser = JSON.parse(localStorage.getItem('snuser'));
      const userId = snuser._id;
      const message = e.target.message.value;

      socketRef.current.emit('chat message', { userId, message });
      e.target.message.value = '';
    } catch (err) {
      console.error(err.message);
    }
  }, []);

  return (
    <div className="Chat">
      <div className="container">
        <h1>Chat</h1>
        <ul className="messages">
          {messages.map(message => {
            return (
              <li key={message._id}>
                <div>
                  <span className="sender">{message.user[0].name} {message.user[0].lastname}: </span>
                  {message.message}
                </div>
                <div>
                  {new Date(message.createdAt).toLocaleString()}
                </div>
              </li>
            );
          })}
        </ul>
        <div className="row">
          <form className="col s12" onSubmit={onFormSubmit}>
            <div className="row">
              <div className="input-field col s12">
                <textarea id="message" name="message" className="materialize-textarea"></textarea>
                <label htmlFor="message">Message</label>
              </div>
              <div className="input-field col s12">
                <button className="btn waves-effect waves-light" type="submit" name="action">
                  Send
                  <i className="material-icons right">send</i>
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default Chat;
