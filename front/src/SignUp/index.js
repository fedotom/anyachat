import './SignUp.css';
import postData from '../utils/postData';
import { useCallback } from 'react';

const SignUp = () => {

  const onFormSubmit = useCallback(async (e) => {
    try {
      e.preventDefault();
  
      const user = {
        name: e.target.name.value,
        lastname: e.target.lastname.value,
        email: e.target.email.value,
        password: e.target.password.value
      };
  
      const response = await postData('/user/signup', user);
  
      if (response.message) {
        window.M.toast({ html: response.message });
      } else if (response.error) {
        window.M.toast({ html: response.error });
      }

    } catch (err) {
      console.error(err);
    }
  }, []);

  return (
    <div className="SignUp">
      <div className="row">
        <h1>Sign Up</h1>
        <form className="col s12" onSubmit={onFormSubmit}>
          <div className="row">
            <div className="input-field col s6">
              <input id="name" name="name" type="text" className="validate" />
              <label htmlFor="name">First Name</label>
            </div>
            <div className="input-field col s6">
              <input id="lastname" name="lastname" type="text" className="validate" />
              <label htmlFor="lastname">Last Name</label>
            </div>
          </div>
          <div className="row">
            <div className="input-field col s12">
              <input id="email" name="email" type="email" className="validate" />
              <label htmlFor="email">Email</label>
            </div>
          </div>
          <div className="row">
            <div className="input-field col s12">
              <input id="password" name="password" type="password" className="validate" />
              <label htmlFor="password">Password</label>
            </div>
          </div>
          <div className="row">
            <div className="input-field col s12">
              <button className="btn waves-effect waves-light" type="submit" name="action">
                Submit
                <i className="material-icons right">send</i>
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};

export default SignUp;
