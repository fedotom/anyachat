import './SignIn.css';
import postData from '../utils/postData';
import { useCallback } from 'react';

const SignIn = () => {

  const onFormSubmit = useCallback(async (e) => {
    try {
      e.preventDefault();
  
      const user = {
        email: e.target.email.value,
        password: e.target.password.value
      };
  
      const response = await postData('/user/signin', user);

      if (response) {
        if (response._id) {
          const { _id, email } = response;
          localStorage.setItem('snuser', JSON.stringify({ _id, email }));
          window.location.href = '/';
        } else if (response.message) {
          window.M.toast({ html: response.message })
        }
      }
    } catch (err) {
      console.error(err);
    }
  }, []);

  return (
    <div className="SignIn">
      <div className="row">
        <h1>Sign In</h1>
        <form className="col s12" onSubmit={onFormSubmit}>
          <div className="row">
            <div className="input-field col s12">
              <input id="email" name="email" type="email" className="validate" />
              <label htmlFor="email">Email</label>
            </div>
          </div>
          <div className="row">
            <div className="input-field col s12">
              <input id="password" name="password" type="password" className="validate" />
              <label htmlFor="password">Password</label>
            </div>
          </div>
          <div className="row">
            <div className="input-field col s12">
              <button className="btn waves-effect waves-light" type="submit" name="action">
                Submit
                <i className="material-icons right">send</i>
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};

export default SignIn;
