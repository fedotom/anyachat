import React, { useEffect, useState } from 'react';
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Link
} from "react-router-dom";
import Chat from './Chat';
import Profile from './Profile';
import SignIn from './SignIn';
import SignUp from './SignUp';

function App() {
  const [isLogin, setIsLogin] = useState(false);

  useEffect(() => {
    setIsLogin(!!localStorage.getItem('snuser'));
  }, []);

  return (
    <div className="App">
      {isLogin && (
        <Router>
          <div>
            <nav>
              <ul>
                <li>
                  <Link to="/">Chat</Link>
                </li>
                <li>
                  <Link to="/profile">Profile</Link>
                </li>
                <li>
                  <Link to="/logout" onClick={() => {
                    localStorage.removeItem('snuser');
                    window.location.href = '/';
                  }}>Logout</Link>
                </li>
              </ul>
            </nav>

            <Routes>
              <Route path="/" element={<Chat />} />
              <Route path="/profile" element={<Profile />} />
            </Routes>
          </div>
        </Router>
      )}

      {!isLogin && (
        <Router>
          <div>
            <nav>
              <ul>
                <li>
                  <Link to="/">Sign In</Link>
                </li>
                <li>
                  <Link to="/signup">Sign Up</Link>
                </li>
              </ul>
            </nav>

            <Routes>
              <Route path="/signup" element={<SignUp />} />
              <Route path="/" element={<SignIn />} />
            </Routes>
          </div>
        </Router>
      )}
      
    </div>
  );
}

export default App;
