const { Router } = require('express');
const router = Router();

const User = require('../models/User');

const signup = async (req, res, next) => {
  try {
    const { name, lastname, email, password } = req.body;
    const user = new User({ name, lastname, email, password });

    await user.save();

    res.json({ message: 'User is successful registered!', ...user });
  } catch (err) {
    if (err) {
      switch (err.code) {
        case 11000:
          res.json({ error: 'User is already exists!' });
          break;
        default:
          res.json({ error: err.message });
          break;
      }
      return next(err);
    }
  }
};

const signin = async (req, res, next) => {
  try {
    const { email, password } = req.body;
    const user = await User.findOne({ email });

    if (user && user.password === password) {
      return res.json(user);
    }

    res.json({ message: 'User is not exists!' });
  } catch (err) {
    if (err) return next(err);
  }
};

const profile = async (req, res, next) => {
  console.log(req.body);
  
  try {
    const { _id, email } = req.body;

    const user = await User.findById(_id);

    if (user && user.email === email) {
      res.json(user);
    } else {
      res.json({ error: 'Something went wrong!' });
    }
  } catch (err) {
    if (err) return next(err);
  }
}

// POST /user/signup
router.post('/signup', signup);
// POST /user/signin
router.post('/signin', signin);
// POST /user/profile
router.post('/profile', profile);

module.exports = router;
