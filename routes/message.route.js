const { Router } = require('express');
const router = Router();

const User = require('../models/User');
const Message = require('../models/Message');

const index = async (req, res, next) => {
  try {
    const messages = await Message.aggregate([
      {
        $lookup: { 
          from: 'users', 
          localField: 'userId', 
          foreignField: '_id', 
          as: 'user' 
        }
      }
    ]);

    res.json(messages);
  } catch (err) {
    if (err) return next(err);
  }
}

const add = async (req, res, next) => {
  try {
    const { userId, message } = req.body;
    const newMessage = new Message({ userId, message });

    await newMessage.save();

    const user = await User.findById(userId);

    res.json({ ...newMessage._doc, user });
  } catch (err) {
    if (err) return next(err);
  };
}

// GET /message
router.get('/', index);
// POST /message/add
router.post('/add', add);

module.exports = router;
module.exports.add = async (msg, io) => {
  try {
    const { userId, message } = msg;
    const newMessage = new Message({ userId, message });

    await newMessage.save();

    const user = await User.findById(userId);

    io.emit('chat message', { ...newMessage._doc, user: [user] });
  } catch (err) {
    if (err) return next(err);
  };
};
