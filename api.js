const { Router } = require('express');

const router = Router();

router.use('/user', require('./routes/user.route'));
router.use('/message', require('./routes/message.route'));

module.exports = router;
